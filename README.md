# Soilmicrobiome_DroughtRewettingChitin

Repository for scripts used for the paper: "The soil microbiome after drought-rewetting and chitin disturbances: the magnitude of the change matters!"

This repository contains:
- R scripts to perform DE analyses
- R scripts to perform statistical analyses for the chemical and PLFA variables
- Supplementary data - results of statistical tests performed on the biochemical data


In case of questions or suggestions regarding the code and statistics, please create an issue on Gitlab.

**Lisa Joos and co-authors**
