---
title: "Comparison of the soils with contrasting soil health using prior data"
author: "Lisa Joos"
date: "`r Sys.Date()`"
output:
  html_document:
    number_sections: yes
    theme: cosmo
    toc: yes
    toc_float: yes
  word_document:
    toc: yes
  pdf_document:
    toc: yes
subtitle: PLFA and chemical variables of prior data
---

Analyses of the previous data of both fields (prior data) to compare the different soil health status of Field A and Field B.
```{r}
# Libraries
library(ggplot2)
library(ggpubr)
library(dplyr)
library(reshape2)
library(plyr)
library(broom)
library(car)
```

```{r include = FALSE}
Publishable_theme <- function() {
   theme(# add axis lines
        axis.line.y = element_line(color = "darkgrey"),
        axis.line.x = element_line(color = "darkgrey"),
        # remove the background
        panel.background = element_blank(), 
        # Add the gridlines of the y-axis
        panel.grid.major.y = element_line(colour = "lightgrey", size = 0.2), 
        panel.grid.minor.y = element_line(colour = "lightgrey", size = 0.2), 
        # remove the vertical lines
        panel.grid.major.x = element_line(colour = "lightgrey", size = 0.2), 
        panel.grid.minor.x = element_line(colour = "lightgrey", size = 0.2), 
        # size of the titles
        axis.title = element_text(size = 8),
        axis.text = element_text(size = 8),
        plot.title = element_text(hjust = 0, size = 8, face="bold"),
        #axis.text.x = element_blank(),
        #remove background legend
        legend.key=element_blank()
        
        )
}
```

```{r}
mySoil <- c("#cc3939",  "#225675")
names(mySoil) <- c("SoilA", "SoilB")
```


# Load data
```{r}
# Define path
path <- './Raw_Data/'

# Metadata, chemical and PLFA data new
ChemicalsPLFA <- read.table(file.path(path, "Metadata/Metadata_PLFA.csv"), header = TRUE, sep = ",", row.names = 1)
```

```{r}
# Clean-up data
## Only keep the two fields and respective treatments for this paper
Soils <- c("Bopact", "Biochar")
Treatments <- c("CS_FC_NIT", "CON")

## Remove the two fields and treatments
ChemicalsPLFA <- ChemicalsPLFA[ChemicalsPLFA$Trial %in% Soils & ChemicalsPLFA$Treatment %in% Treatments,]

# Redefine the two used soils to the names for the manuscript
ChemicalsPLFA$Trial <- revalue(ChemicalsPLFA$Trial, c("Biochar"="SoilA", "Bopact"="SoilB"))

# Reshape the dataset from wide to long using melt
ChemicalsPLFA.long <- melt(ChemicalsPLFA, id.vars = c("ID_Fungi", "ID_Executor", "Trial", "Trial_Block", "Date_Ch_analyses", "Date_PLFA_analyses", "Behandeling", "Treatment", "Nitrogen", "Min_fertilizer", "Digestate", "Compost", "Biochar", "Tillage", "Crop", "Blok", "Plot", "Depth", "DS", "Year"))

# Reduce the dataset
ChemicalsPLFA.long <- data.frame("Trial" = ChemicalsPLFA.long$Trial, "variable" = ChemicalsPLFA.long$variable, "value" = as.numeric(ChemicalsPLFA.long$value))
```

```{r}
# Create dataset only containing the PLFAs
CheckVar <- c("Total_non_spec_bact", "Total_gram_pos_bact", "Total_actino", "Total_gram_neg_bact",	"Total_AM", "Total_nonAM_fungi",	"Total_bact",	"Total_fungi", "Total_biomass", "Bfratio")

PLFA <- ChemicalsPLFA.long[ChemicalsPLFA.long$variable %in% CheckVar,]

# Create dataset only containing the chemicals
# Na_AL < detection limit
CheckChemicals <- c("HWC", "EC", "HWP", "TOC", "pH", "Total_N", "C.N", "P_AL", "P_CaCl2", "K_AL", "Mg_AL", "Ca_AL", "Fe_AL", "Mn_AL", "Fe_AL")

Chemicals <- ChemicalsPLFA.long[ChemicalsPLFA.long$variable %in% CheckChemicals,]
```

# Exploration of the two soils

## PLFA
```{r}
# Plot richness indices Part P1
ggplot(PLFA, aes(x = variable , y = as.integer(value), colour = Trial, fill = Trial)) +
   geom_boxplot(alpha = 0.5, outlier.shape = 8, outlier.colour = "black") + # indicate outliers
   geom_point(position=position_jitterdodge()) +
   coord_flip() +
   guides(fill = FALSE) +
        
   # Set lay out of the graph
  Publishable_theme() +
  theme(axis.text = element_text(size=10),
         axis.title = element_text(size=12) ,
         axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1)) +
  
  scale_fill_manual(values = mySoil) +
  scale_colour_manual(values =  mySoil) +
  xlab("") +
  ylab("PLFA") +  ylab(expression("Absolute concentration nmol" ~g^{"-1"}~"dry soil")) +
  ggtitle("PLFA")
```

## Chemical properties
```{r}
# Plot richness indices Part P1
ggplot(Chemicals[!Chemicals$variable == "HWC",], aes(x = variable , y = as.numeric(value), colour = Trial, fill = Trial)) +
   geom_boxplot(alpha = 0.5, outlier.shape = 8, outlier.colour = "black") + # indicate outliers
   geom_point(position=position_jitterdodge()) +
   coord_flip() +
   guides(fill = FALSE) +
        
   # Set lay out of the graph
  Publishable_theme() +
  theme(axis.text = element_text(size=10),
         axis.title = element_text(size=12) ,
         axis.text.x = element_text(angle = 45, vjust = 1, hjust = 1)) +
  
  scale_fill_manual(values = mySoil) +
  scale_colour_manual(values =  mySoil) +
  xlab("") +
  ylab("PLFA") +  ylab(expression("Absolute concentration nmol" ~g^{"-1"}~"dry soil")) +
  ggtitle("PLFA")
```

## Summary PLFA and chemical data
```{r}
# Show average of Part 2
SummaryChemicals <- Chemicals %>%
  group_by(Trial, variable) %>%
  dplyr::summarize(Mean = mean(as.numeric(value)),
            n = n(),
             SD = sd(as.numeric(value)),
             CVperc = (SD/Mean)*100,
             SE = sd(value)/sqrt(n),
             CI.lower = Mean - 2*SE,
             CI.upper = Mean + 2*SE) %>%
  mutate_if(is.numeric, round, digits = 2)

write.table(SummaryChemicals, "PreviousData_soils.csv")

# PLFA
SummaryPLFA <- PLFA %>%
  group_by(Trial, variable) %>%
  dplyr::summarize(Mean = mean(as.numeric(value)),
            n = n(),
             SD = sd(as.numeric(value)),
             CVperc = (SD/Mean)*100,
             SE = sd(value)/sqrt(n),
             CI.lower = Mean - 2*SE,
             CI.upper = Mean + 2*SE) %>%
  mutate_if(is.numeric, round, digits = 2)

write.table(SummaryPLFA, "PreviousDataPLFA_soils.csv")
```

# Simple t.test PLFA
```{r}
# Check assumptions
## Test Shapiro-Wilk normality test # allemaal ok
shapiro.check <- PLFA %>%
  group_by(variable, Trial) %>% 
 do(tidy(shapiro.test(.$value)))

# Levene's test
## Homogeneity of variances # Not ok for Total nonAM (p = 0.044)
levene.check <- PLFA %>%
  group_by(variable) %>% 
 do(tidy(leveneTest(value ~ Trial, data = .)))


### Issue for total non specific bacteria and total non AM fungi
boxplot(PLFA$value[PLFA$variable == "Total_nonAM_fungi"] ~ PLFA$Trial[PLFA$variable == "Total_nonAM_fungi"])     
boxplot(PLFA$value[PLFA$variable == "Total_non_spec_bact"] ~ PLFA$Trial[PLFA$variable == "Total_non_spec_bact"])  
```

```{r}
# t.test unpaired data
PLFA_equalvariances <- PLFA[PLFA$variable != c("Total_nonAM_fungi"),]

stat.test <- PLFA_equalvariances %>%
  group_by(variable) %>% 
  do(tidy(t.test(value ~ Trial, data = ., paired = FALSE, var.equal = TRUE)))


stat.test$variable[stat.test$p.value < 0.01]

write.table(stat.test, "Output/Prior_data/PLFA_equalvariances.csv")
```


*pvalues smaller than 0.001*
Actinomycetes
Gram neg
AM
total bact

*pvalues smaller than 0.01*
Total biomass
BF ratio

*pvalues smaller than 0.05*
non spec bact
gram pos

*not significant*
Total nonAM fungi
Total fungi


```{r}
# Welch t-test  for unequal variances for non_am fungi
## Remark total nonAM fungi are not included in the manuscript.
PLFA_noequalvariances <- PLFA[PLFA$variable == c("Total_nonAM_fungi"),]
stat.test <- PLFA_noequalvariances %>%
  group_by(variable) %>% 
  do(tidy(t.test(value ~ Trial, data = ., paired = FALSE, var.equal = FALSE)))
```


# Simple t.test chemicals
```{r}
#Check assumptions
## Test Shapiro-Wilk normality test # allemaal ok
shapiro.check <- Chemicals %>%
  group_by(variable, Trial) %>% 
 do(tidy(shapiro.test(.$value)))

# Levene's test
## Homogeneity of variances
levene.check <- Chemicals %>%
  group_by(variable) %>% 
 do(tidy(leveneTest(value ~ Trial, data = .)))

# Variances not normal for Total nitrogen, Mg, Ca
## Plot
boxplot(Chemicals$value[Chemicals$variable == "Total_N"] ~ Chemicals$Trial[Chemicals$variable == "Total_N"]) 
boxplot(Chemicals$value[Chemicals$variable == "Mg_AL"] ~ Chemicals$Trial[Chemicals$variable == "Mg_AL"]) 
boxplot(Chemicals$value[Chemicals$variable == "Ca_AL"] ~ Chemicals$Trial[Chemicals$variable == "Ca_AL"]) 
```

```{r}
Chemicals_equalvariances <- Chemicals[!Chemicals$variable %in% c("Total_N", "Mg_AL", "Ca_AL"),]
stat.test <- Chemicals_equalvariances %>%
  group_by(variable) %>% 
  do(tidy(t.test(value ~ Trial, data = ., paired = FALSE, var.equal = TRUE)))

stat.test$variable[stat.test$p.value > 0.05]

write.table(stat.test, "Output/Prior_data/Chemicals_equalvariances.csv")
```

*P < 0.001*
K

*P < 0.01*
HWC
TOC

C.N
Mn

*P < 0.05*
HWP
Total_N 

*P > 0.05*
EC
pH
P
Fe

```{r}
Chemicals_noequalvariances <- Chemicals[Chemicals$variable %in% c("Total_N", "Mg_AL", "Ca_AL"),]

stat.test <- Chemicals_noequalvariances %>%
  group_by(variable) %>% 
  do(tidy(t.test(value ~ Trial, data = ., paired = FALSE, var.equal = FALSE)))

write.table(stat.test, "Output/Prior_data/Chemicals_noequalvariances.csv")
```





